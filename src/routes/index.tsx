import React from 'react';
import {Button} from 'react-native';
import {createStackNavigator} from '@react-navigation/stack';
import HomeScreen from '../screens/home_screen';
import RegisterScreen from '../screens/register_screen';

const Stack = createStackNavigator();

const AppRoutes = () => {
  return (
    <Stack.Navigator initialRouteName="Home">
      <Stack.Screen
        name="Home"
        component={HomeScreen}
        options={{
          headerTitle: 'Tela Inicial',
          headerTitleAlign: 'center',
          //   headerRight: () => <Button title="+" onPress={() => {}} />,
        }}
      />
      <Stack.Screen
        name="Register"
        component={RegisterScreen}
        options={{
          headerTitle: 'Registrar Empregado',
          headerTitleAlign: 'center',
        }}
      />
    </Stack.Navigator>
  );
};

export default AppRoutes;
