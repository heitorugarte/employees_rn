import React from 'react';
import {Container} from './styles';
import EmployeeCard from '../../components/employee_card';
import {Employee} from '../../types/employee';
import {FlatList} from 'react-native';

const employee: Employee = {
  id: 0,
  employee_age: 22,
  employee_name: 'Heitor Ugarte Calvet da Silveira',
  employee_salary: 3300,
};

const employeeData: Employee[] = [employee, employee];

const HomeScreen = () => {
  return (
    <Container>
      <FlatList
        data={employeeData}
        renderItem={(item) => <EmployeeCard employee={item.item} />}
        keyExtractor={(item) => item.id.toString()}
      />
    </Container>
  );
};

export default HomeScreen;
