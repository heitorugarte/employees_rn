import {ColorValue} from 'react-native';

export type FontSizes = {
  small: number;
  medium: number;
  large: number;
};

export const fontSizes: FontSizes = {
  small: 14,
  medium: 18,
  large: 24,
};

export type FontColors = {
  white: string;
  black: string;
};

export const fontColors: FontColors = {
  white: '#fff',
  black: '#000',
};
