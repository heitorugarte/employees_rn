import React from 'react';
import {Employee} from '../../types/employee';
import {TextLabel, TextLabelBold} from '../text_label';
import {Container, FieldContainer} from './styles';

interface IEmployeeCard {
  employee: Employee;
}

const EmployeeCard: React.FC<IEmployeeCard> = ({employee}) => {
  return (
    <Container>
      <FieldContainer>
        <TextLabelBold>ID:</TextLabelBold>
        <TextLabel>{employee.id}</TextLabel>
      </FieldContainer>
      <FieldContainer>
        <TextLabelBold>Idade:</TextLabelBold>
        <TextLabel>{employee.employee_age}</TextLabel>
      </FieldContainer>
      <FieldContainer>
        <TextLabelBold>Nome:</TextLabelBold>
        <TextLabel>{employee.employee_name}</TextLabel>
      </FieldContainer>
      <FieldContainer>
        <TextLabelBold>Salário:</TextLabelBold>
        <TextLabel>{employee.employee_salary}</TextLabel>
      </FieldContainer>
    </Container>
  );
};

export default EmployeeCard;
