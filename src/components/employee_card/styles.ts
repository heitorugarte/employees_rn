import styled from 'styled-components/native';

export const Container = styled.View`
  width: 100%;
  padding: 20px;
  border-radius: 10px;
  elevation: 1;
  margin-bottom: 10px;
`;

export const FieldContainer = styled.View`
  flex-direction: row;
  justify-content: space-between;
`;
