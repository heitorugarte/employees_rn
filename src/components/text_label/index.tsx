import styled from 'styled-components/native';
import {fontSizes, fontColors} from '../../theme/fonts';

export const TextLabel = styled.Text`
  font-size: ${fontSizes.small};
  color: ${fontColors.black};
`;

export const TextLabelBold = styled.Text`
  font-size: ${fontSizes.small};
  color: ${fontColors.black};
  font-weight: bold;
`;
